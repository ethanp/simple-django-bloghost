from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()


urlpatterns = patterns('blog.views',


#   1) MySite HomePage listing Blogs
#       a) allows Create New Blog
    url(r'^blogs/$', 'index'),


#   2) HomePage forEach Blog:
#       a) Lists all Posts inOrder
    url(r'^blogs/(?P<blog_id>\d+)/$', 'author'),


#   3) Site forEach Post
#       a) Comments @the bottom
#       b) allows Create New Comment
    url(r'^posts/(?P<post_id>\d+)/$', 'post'),


#   4) Post Comment
    url(r'^posts/(?P<post_id>\d+)/comment/$', 'leave_comment'),


#   5) New BlogPost
    url(r'^blogs/(?P<blog_id>\d+)/new/$', 'new_post'),


#   6) New BlogSite
    url(r'^blogs/create/$', 'new_blog'),
)

urlpatterns += patterns('',
    url(r'^admin/', include(admin.site.urls)),
)
