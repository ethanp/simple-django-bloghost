from blog.models import Blog, BlogForm, Post, PostForm, Comment, CommentForm
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.core.urlresolvers import reverse
from django.template import RequestContext, Context, loader
from django.shortcuts import get_object_or_404, render_to_response, render
 
# "Each View must instantiate, populate, and return an HttpResponse


def index(request):
    latest_blog_list = Blog.objects.all().order_by('title')[:5]
    return render_to_response('blogs/index.html', { 'latest_blog_list': latest_blog_list, })


def new_blog(request):

    # This is for saving the data AFTER they submit it in the ELSE statement
    if request.method == 'POST': # if the form has been submitted
        form = BlogForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            # Process the data in form.cleaned_data
            title = form.cleaned_data['title']
            author = form.cleaned_data['author']
            form.save()
            try:
                latest_blog_list = Blog.objects.all().order_by('title')[:5]
            except Blog.DoesNotExist:
                raise Http404
            return render_to_response('blogs/index.html', {'latest_blog_list': latest_blog_list })

        # This is for SHOWING THE FORM, it is NOT an ERROR!
        else:
            form = BlogForm() # An unbound form
            try:
                latest_blog_list = Blog.objects.all().order_by('title')[:5]
            except Blog.DoesNotExist:
                raise Http404
        return render_to_response('blogs/newblog.html', {'form': form, 'latest_blog_list': latest_blog_list})


#   I need this to show the title of this post
def author(request, blog_id):
    b = get_object_or_404(Blog, id=blog_id)
    authors_latest_posts = b.post_set.all().order_by('-published')[:5]
    return render_to_response('blogs/authors.html', { 'blog': b, 'authors_latest_posts': authors_latest_posts})


def new_post(request, blog_id):

    # This is for saving the data AFTER they submit it in the ELSE statement
    if request.method == 'POST': # if the form has been submitted
        form = PostForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            # Process the data in form.cleaned_data
            blog = form.cleaned_data['blog']
            title = form.cleaned_data['title']
            published = form.cleaned_data['published']
            text = form.cleaned_data['text']
            form.save()
            b = get_object_or_404(Blog, blog_id)
            try:
                authors_latest_posts = b.post_set.all().order_by('-published')[:5]
            except Blog.DoesNotExist:
                raise Http404
            return render_to_response('blogs/authors.html', {'blog': b,
                                                             'authors_latest_posts': authors_latest_posts})

        # This is for SHOWING THE FORM, it is NOT an ERROR!
        else:
            form = PostForm(initial={'blog': blog_id}) # An unbound form
            b = Blog.objects.get(id=blog_id)
            authors_latest_posts = b.post_set.all().order_by('-published')[:5]
        return render_to_response('blogs/newpost.html', {'form': form, 'blog': b,
                                                         'authors_latest_posts': authors_latest_posts})


def post(request, post_id):
    p = get_object_or_404(Post, id=post_id)
    comment_list = p.comment_set.all()[:5]
    return render_to_response('blogs/posts.html', {'post': p, 'comment_list': comment_list, })


def leave_comment(request, post_id):

    # This is for saving the data AFTER they submit it in the ELSE statement
    if request.method == 'POST': # if the form has been submitted
        form = CommentForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            # Process the data in form.cleaned_data
            user = form.cleaned_data['user']
            text = form.cleaned_data['text']
            post = form.cleaned_data['post']
            form.save()
            try:
                p = Post.objects.get(id=post_id)
                comment_list = p.comment_set.all()[:5]
            except Post.DoesNotExist:
                raise Http404
            return render_to_response('blogs/posts.html', {'post': p, 'comment_list': comment_list, })

        # This is for SHOWING THE FORM, it is NOT an ERROR!
        else:
            form = CommentForm(initial={'post': post_id}) # An unbound form
            p = Post.objects.get(id=post_id)
        return render_to_response('blogs/comment.html', { 'form': form, 'post': p })

