from blog.models import Blog, Post, Comment
from django.contrib import admin


class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'blog', 'was_published_recently')
    date_hierarchy = 'published'


class BlogAdmin(admin.ModelAdmin):
    list_display = ('title', 'author')


class CommentAdmin(admin.ModelAdmin):
    list_display = ('post', 'text')


admin.site.register(Blog, BlogAdmin)
admin.site.register(Post, PostAdmin)
admin.site.register(Comment, CommentAdmin)
