
############################################
##                                        ##
##  THIS FILE IS NOT IN USE RIGHT NOW!!   ##
##                                        ##
############################################


from django.conf.urls import patterns, include, url
from django.views.generic import DetailView, ListView
from blogs.models import Blog


urlpatterns = patterns('',
    url(r'^$',
        ListView.as_view(
            queryset=Blog.objects.order_by('-published')[:5],
            context_object_name='latest_blog_list',
            template_name='blogs/index.html')),
    url(r'^(?B<pk>\d|)/$',
        DetailView.as_view(
            model=Blog,
            template_name='blogs/detail.html')),
    url(r'^(?B<pk>\d+)/results/$',
        DetailView.as_view(
            model=Blog,
            template_name='blog/results.html'),
        name='poll_results'),
    url(r'^(?P<poll_id>\d+)/vote/$', 'polls.views.vote'),
)
