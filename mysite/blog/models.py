from django.db import models
from django.forms import ModelForm, Textarea
import datetime
from django.utils import timezone
from django import forms

class Blog(models.Model):
    title = models.CharField(max_length=50)
    author = models.CharField(max_length=50)

    def __unicode__(self):
        return self.title


class Post(models.Model):
    blog = models.ForeignKey(Blog, verbose_name="of blogger")
    title = models.CharField(max_length=100)
    published = models.DateTimeField(default=datetime.datetime.now)
    text = models.TextField()

    def was_published_recently(self):
        return self.published >= timezone.now() - datetime.timedelta(days=1)
    was_published_recently.admin_order_field = 'published'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Still Fresh?'

    def __unicode__(self):
        return self.title


class Comment(models.Model):
    post = models.ForeignKey(Post)
    user = models.CharField(max_length=25)
    text = models.TextField()

    def __unicode__(self):
        return self.user


class BlogForm(ModelForm):
    class Meta:
        model = Blog


class PostForm(ModelForm):
    class Meta:
        model = Post


class CommentForm(ModelForm):
    class Meta:
        model = Comment

